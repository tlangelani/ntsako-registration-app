<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/app.css">
</head>

<body>
    <h2 align="center" ; style="font-size:300%;color:grey;"><em>Welcome......Please enter your details to register</em></h2>

    <form action="process.php" method="post">
        <div class="imgcontainer">
            <img src="reg.jpg">
        </div>

        <div class="container">

            <label><b>NAME</b></label>
            <input type="text" placeholder="Enter your Name" name="name" required>

            <label><b>SURNAME</b></label>
            <input type="text" placeholder="Enter your Surname" name="sname" required>

            <label><b>GENDER</b></label>
            <input type="text" placeholder="Enter your Gender" name="gender" required>

            <label><b>AGE</b></label>
            <input type="text" placeholder="Enter your Age" name="age" required>

            <label><b>CELLPHONE NUMBER</b></label>
            <input type="text" placeholder="Enter your Cellphone number" name="phone" required>

            <label><b>PASSWORD</b></label>
            <input type="password" placeholder="Enter Password" name="psw" required>

            <label><b>CONFIRM PASSWORD</b></label>
            <input type="password" placeholder="Enter Password confirmation" name="psw1" required>

            <button type="submit" name='btnReg'>Register</button>

        </div>

        <div class="container" style="background-color:#f1f1f1">
            <button type="submit" name="btncancel" id="btncancel">Cancel</button>
        </div>
    </form>

    <script src="/js/app.js"></script>
</body>

</html>
